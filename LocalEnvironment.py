

# Machine environment dependent variables

# Typically, this is where you have the various python and NCL scripts located
SOUNDING_EVAL_ROOT_DIR = '/u1/uaf/morton/WX/AshSounding'

########################################################################
### NOTE, the current implementation of this script will assume, and return 
### dimensions of meters, hPa, degrees C, kgkg-1, m/s, and degrees (rotated to 
### Earth coordinates
NCL_EXTRACT_SCRIPT = SOUNDING_EVAL_ROOT_DIR + '/ExtractWRFSounding.ncl'
########################################################################

NCL_SKEWT_SCRIPT = SOUNDING_EVAL_ROOT_DIR + '/DualSoundingPlot.ncl'

# Stuff needed for NCL
NCARG_ROOT = '/usr/local/pkg/ncl/ncl-5.1.1.gnu'
NCL_COMMAND = NCARG_ROOT + '/bin/ncl'

CONVERT_BIN = '/usr/bin/convert'  # from ImageMagick

# A number of temporary files and directories will be created.  This
# specifies where.
WORKING_DIR_ROOT = '/center/w/morton/HRRR-AK/Scratch'

### These should correspond to stations listed in Stations.py
STATION_LIST = ['PAFA', 'PANC', 'PAJN', 'PABR', 'PAOM']

###########################################################
### If START_TIME is None, then it will be calculated based on command
### line arguments of cycle time and days offset.  For example, if START_TIME
### is set to None, and the current date is 2009-05-14, then one might invoke 
### the script like
###
###     ./SoundingVerify.py  12  -4
###
### and the script would calculate a forecast start time of 2009-05-10_12Z
###
###########################################################
START_TIME = '2012102500'
#START_TIME = None


# The root locaion of wrfout files.  The script will look for wrfout files in 
# WRFOUT_ROOT_DIR + '/' + START_TIME. 
WRFOUT_ROOT_DIR = '/projects/ARSCWTHR/fromPacman/Operational/HRRR-AK/Products/wrfout'

NEST_NUMBER = 1

# Where the products will go.  Underneath this directory, a sequence of 
# subdirectories will be created.  If forecast start time is YYYYMMDDHH, then
# the directory tree will be
#
#     YYYY/MM/YYYY-MM-DD_HH/StationID
#
# For each station ID, all tables and images will be located, along with an
# index.html for viewing with a browser.
#
PRODUCT_ROOT_DIR = '/center/w/morton/TEMP'





