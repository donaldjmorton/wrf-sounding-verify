

###############################################################
#                                                             #
#    Original Author contact information                      #
#                                                             #
#    Don Morton                                               #
#    Arctic Region Supercomputing Center                      #
#    University of Alaska Fairbanks                           #
#    Fairbanks, Alaska 99775                                  #
#    USA                                                      #
#                                                             #
#    Don.Morton@alaska.edu                                    #
#                                                             #
#    This software is presented freely to the public with     #
#    no restrictions on its use.  However, it would be        #
#    appreciated if any use of the software or methods in     #
#    part or in full acknowledges the source.                 #
#                                                             #
###############################################################



import os
import socket
import subprocess
import sys
import time
import urllib

import LocalEnvironment


class RaobsSounding:

    # If a raobs fetch fails, how long will we wait before
    # trying again, and how many times will we try?
    _RAOBS_BUSY_WAIT_SECONDS = 30
    _RAOBS_MAX_FETCH_ATTEMPTS = 10
    _RAOBS_SOCKET_TIMEOUT_SECONDS = 60

    _WORKING_DIR_ROOT = LocalEnvironment.WORKING_DIR_ROOT
    

    def __init__(self, raobsID, timestamp):

        # For now, assume that raobsID is 5-digit numeric
        # timestamp is numeric, 10-digit, YYYYMMDDHH

        # This list stores the individual record lines of the soundings obtained from
        # URL
        self._theRawRaobsSounding = []

        # Lists of the sounding variables
        self._z = []        
        self._P = []       
        self._T = []        
        self._Td = []       
        self._Qv = []       
        self._Wv = []     
        self._Wd = []       




        allOK = True

        # Check raobsID for proper format
        strID = str(raobsID)
        if len(strID) == 5 and strID.isdigit():
            print 'RaobsSounding(): raobsID looks good: ' + strID
        else:
            print 'RaobsSounding(): bad raobsID format: ' + strID
            allOK = False

        # Check timestamp proper format
        strTimestamp = str(timestamp)
        if len(strTimestamp) == 10 and strTimestamp.isdigit():
            year = strTimestamp[0:4]
            month = strTimestamp[4:6]
            day = strTimestamp[6:8]
            hour = strTimestamp[8:10]

            if int(year) < 1970 or int(year) > 2050:
                print 'RaobsSounding(): year out of range: ' + year
                allOK = False
            if int(month) < 1 or int(month) > 12:
                print 'RaobsSounding(): month out of range: ' + month
                allOK = False
            if int(day) < 1 or int(day) > 31:
                print 'RaobsSounding(): day out of range: ' + day
                allOK = False
            if int(hour) < 0 or int(hour) > 23:
                print 'RaobsSounding(): hour out of range: ' + hour
                allOK = False
            if hour not in ['00','12']:
                print 'RaobsSounding(): hour is not 00 or 12: ' + hour
                allOK = False

        else:
            print 'RaobsSounding(): bad timestamp format: ' + strTimestamp
            print '                 Expecting 10 digits'
            allOK = False
            
        if allOK:
            self._stationID = strID
            self._year = year 
            self._month = month 
            self._day = day 
            self._hour = hour 
            print 'RaobsSounding.py()'
            print '    Station ID: ' + self._stationID
            print '    Timestamp : ' + strTimestamp
            self._extractRaobsSounding()

#-------------------------------------------------------------

    def _extractRaobsSounding(self):

        #-----------------------
        def busyWait(seconds, reason):
            # Puts process to sleep for "seconds" secs, stating "reason" first

            print 'busyWait() for ' + str(seconds) + ' seconds'
            print '    reason: ' + reason
            print ' '
            time.sleep(seconds)
        #-----------------------

        def fetchRaobs(theURL):



            print 'RaobsSounding(): Retrieving: ' + theURL

    
            socket.setdefaulttimeout(self._RAOBS_SOCKET_TIMEOUT_SECONDS)
            urlFH = urllib.urlopen(theURL)

            raobsLines = []
            for line in urlFH:
                raobsLines.append(line)
            numRaobsLines = len(raobsLines)

            urlFH.close()



            ### Extract the relevant sounding lines from the raw HTML lines and store in list
            done = False
            idx = 6     # Initialize to the first data line in the HTML (skipping the HTML)
            while not done and idx < numRaobsLines:
                ###  NOTE - sometimes I don't get a complete HTML, and idx can go out of bounds before 
                ###  coming to the sentinel line (</PRE>...).  So, in the while condition, I'm also 
                ###  insuring that idx won't exceed numRaobsLines

                theLine = raobsLines[idx]
                theTokens = theLine.split()
                if len(theTokens) > 0 and theTokens[0] == '</PRE><H3>Station':
                    done = True
                else:
                    self._theRawRaobsSounding.append(theLine)
                
                idx = idx + 1

            # Now, go ahead and store the data
            lineNum = 0
            for line in self._theRawRaobsSounding:
                if lineNum > 4:
                    #print line
                    theTokens = line.split()
                    # We expect 11 tokens.  If not present, skip to next line
                    if len(theTokens) == 11:
                        ##########################################
                        ##### HACK - 2010-08-04  ####
                        #
                        # Sometimes, if an incorrect URL is sent (e.g. one for
                        # a sounding that does not yet exist), a returned line
                        # just might have 11 tokens, and then the conversion
                        # of the token to float() can fail.  This hack ASSUMES
                        # that if theToken[0] cannot be converted, then the 
                        # whole line should be discarded.
                        #
                        # Note that we "could" still have a bad line, where
                        # theToken[0] can actually be converted to a float.  In
                        # such a case, this hack won't catch the problem,
                        # and the routine will ABEND.
                        ##########################################
                        stillOK = True
                        try:
                            f = float(theTokens[0])
                        except ValueError, e:
                            print 'fetchRaobs(): theTokens[0] cannot be converted to float()'
                            print '    Skipping the rest of the line...'
                            stillOK = False
                        if stillOK:
                            self._P.append( float(theTokens[0]) )
                            self._z.append( float(theTokens[1]) )
                            self._T.append( float(theTokens[2]) )
                            self._Td.append( float(theTokens[3]) )
                            self._Qv.append( float(theTokens[5]) )
                            self._Wd.append( float(theTokens[6]) )
                            self._Wv.append( float(theTokens[7]) )
                    else:
                        print 'RaobsSounding(): incomplete sounding Level ' + \
                              str(lineNum) + ', skipping...'
               
                lineNum += 1

        #-----END fetchRaobs()------
















        # Create a temporary working directory
        wrkdirbasename = os.path.basename( os.tempnam() )
        wrkdirname = self._WORKING_DIR_ROOT + '/' + wrkdirbasename
        print 'RaobsSounding(): Created wrkdirname: ' + wrkdirname
        os.mkdir( wrkdirname )


        # Change to the temporary working directory
        if os.path.isdir(wrkdirname):
            os.chdir(wrkdirname)
        else:
            print 'RaobsSounding(): Temporary work directory not found: ' + wrkdirname
            sys.exit(1)

        # Create the URL for the raobs data
        theURL = "http://weather.uwyo.edu/cgi-bin/sounding?region=naconf&"
        theURL += "TYPE=TEXT%3ALIST&YEAR=" + self._year + "&MONTH=" + self._month
        theURL += "&FROM=" + self._day + self._hour + "TO=" + self._day + self._hour
        theURL += "&STNM=" + self._stationID


        success = False
        numTries = 0
        while not success and numTries < self._RAOBS_MAX_FETCH_ATTEMPTS:
            fetchRaobs(theURL)
            if self.getNumLevels() > 0:
                success = True
            else:
                theMessage = 'Raobs fetch failed, attempt ' + str(numTries)
                busyWait(self._RAOBS_BUSY_WAIT_SECONDS, theMessage)

            numTries += 1

#-------------------------------------------------------------

    def printASCIITable(self, outputFileHandle):

        for line in self._theRawRaobsSounding:
            outputFileHandle.write(line)

        
#-------------------------------------------------------------

    def getz(self):
        return self._z

    def getP(self):
        return self._P

    def getP(self):
        return self._P

    def getT(self):
        return self._T

    def getTd(self):
        return self._Td

    def getQv(self):
        return self._Qv

    def getWv(self):
        return self._Wv

    def getWd(self):
        return self._Wd

    def getNumLevels(self):
        return len(self._P)

    def getPBottom(self):
        # Returns pressure level of bottom layer in
        # stored sounding
        if self.getNumLevels() > 0:
            return self._P[0]
        else:
            return 0

    def getPTop(self):
        # Returns pressure level of top layer in   
        # stored sounding
        if self.getNumLevels() > 0:
            return self._P[ len(self._P) - 1 ]
        else:
            return 0

#-------------------------------------------------------------
    



