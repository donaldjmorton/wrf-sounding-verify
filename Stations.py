

###############################################################
#                                                             #
#    Original Author contact information                      #
#                                                             #
#    Don Morton                                               #
#    Arctic Region Supercomputing Center                      #
#    University of Alaska Fairbanks                           #
#    Fairbanks, Alaska 99775                                  #
#    USA                                                      #
#                                                             #
#    Don.Morton@alaska.edu                                    #
#                                                             #
#    This software is presented freely to the public with     #
#    no restrictions on its use.  However, it would be        #
#    appreciated if any use of the software or methods in     #
#    part or in full acknowledges the source.                 #
#                                                             #
###############################################################





#            StationID : (RaobsID, Lat, Lon,  Elev(m), Location String)
_StationList = {
              'PABE' : (70219, 60.78, -161.80,   46.0, "Bethel"),
              'PABR' : (70026, 71.30, -156.78,    4.0, "Barrow"),
              'PACD' : (70316, 55.20, -162.71,   31.0, "Cold Bay"),
              'PADQ' : (70350, 57.75, -152.50,   34.0, "Kodiak"),
              'PAFA' : (70261, 64.81, -147.86,  138.0, "Fairbanks"),
              'PAKN' : (70326, 58.68, -156.65,   15.0, "King Salmon"),
              'PAMC' : (70231, 62.96, -155.61,  103.0, "McGrath"),
              'PANC' : (70273, 61.16, -150.01,   40.0, "Anchorage"),
              'PANT' : (70398, 55.03, -131.56,   34.0, "Annette Island"),
              'PAOM' : (70200, 64.50, -165.43,    7.0, "Nome"),    
              'PAOT' : (70133, 66.86, -162.63,    5.0, "Kotzebue"),
              'PASN' : (70308, 57.15, -170.21,    9.0, "St. Paul"),
              'PASY' : (70414, 52.71, +174.10,   31.0, "Shemya"),
              'PAYA' : (70361, 59.51, -139.66,    9.0, "Yakutat"),
              'YXY'  : (71964, 60.70, -135.06,  704.0, "Whitehorse"),
              'YZT'  : (71109, 50.68, -127.36,   17.0, "Port Hardy")
              }


class Stations:

    "Interface to static data on sounding sites"

    def isPresent(self, sid):
        # Returns True if sid is in the dictionary, otherwise false
        if _StationList.has_key(sid):
            returnValue = True
        else:
            returnValue = False

        return returnValue


   
    def raobsID(self, sid):
        if _StationList.has_key(sid):
            theTuple = _StationList[sid]
            returnVal = theTuple[0]
        else:
            print 'Stations.raobsID(): Station ID not found: ' + sid
            returnVal = None

        return returnVal


    def latitude(self, sid):
        if _StationList.has_key(sid):
            theTuple = _StationList[sid]
            returnVal = theTuple[1]
        else:
            print 'Stations.latitude(): Station ID not found: ' + sid
            returnVal = None

        return returnVal

    def longitude(self, sid):
        if _StationList.has_key(sid):
            theTuple = _StationList[sid]
            returnVal = theTuple[2]
        else:
            print 'Stations.longitude(): Station ID not found: ' + sid
            returnVal = None

        return returnVal


    def elevation(self, sid):
        if _StationList.has_key(sid):
            theTuple = _StationList[sid]
            returnVal = theTuple[3]
        else:
            print 'Stations.elevation(): Station ID not found: ' + sid
            returnVal = None

        return returnVal

    def locationString(self, sid):
        if _StationList.has_key(sid):
            theTuple = _StationList[sid]
            returnVal = theTuple[4]
        else:
            print 'Stations.locationString(): Station ID not found: ' + sid
            returnVal = None

        return returnVal



    def printStation(self, sid):
        if _StationList.has_key(sid):
            theTuple = _StationList[sid]
            print str(theTuple)
        else:
            print 'Stations.printStation(): Station ID not found: ' + sid





    def printAllStations(self):
        for item in _StationList.iteritems():
            print str(item)




