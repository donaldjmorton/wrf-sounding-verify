
###############################################################
#                                                             #
#    Original Author contact information                      #
#                                                             #
#    Don Morton                                               #
#    Arctic Region Supercomputing Center                      #
#    University of Alaska Fairbanks                           #
#    Fairbanks, Alaska 99775                                  #
#    USA                                                      #
#                                                             #
#    Don.Morton@alaska.edu                                    #
#                                                             #
#    This software is presented freely to the public with     #
#    no restrictions on its use.  However, it would be        #
#    appreciated if any use of the software or methods in     #
#    part or in full acknowledges the source.                 #
#                                                             #
###############################################################


import os
import subprocess
import sys

import LocalEnvironment


class WRFSounding:

    _NCL_EXTRACT_SCRIPT = LocalEnvironment.NCL_EXTRACT_SCRIPT
    _NCARG_ROOT = LocalEnvironment.NCARG_ROOT
    _NCL_COMMAND = LocalEnvironment.NCL_COMMAND
    _WORKING_DIR_ROOT = LocalEnvironment.WORKING_DIR_ROOT
    
    def __init__(self, wrfoutFile, stationLat, stationLon):

        # Start with empty lists
        # This list stores the individual lines of the text file produced by
        # the WRF sounding NCL extraction script
        self._theRawAsciiSounding = []

        # Lists of the sounding variables
        self._z = []
        self._P = []
        self._T = []
        self._Td = []
        self._Qv = []
        self._Wv = []
        self._Wd = []
        allOK = True

        if not os.path.isfile(self._NCL_EXTRACT_SCRIPT):
            print 'WRFSounding(): _NCL_EXTRACT_SCRIPT not found: ' + self._NCL_EXTRACT_SCRIPT
            print '      Define this in WRFSounding.py'
            allOK = False
        
        if not os.path.isdir(self._NCARG_ROOT):
            print 'WRFSounding(): _NCARG_ROOT not found: ' + self._NCARG_ROOT
            print '      Define this in WRFSounding.py'
            allOK = False

        if not os.path.isfile(wrfoutFile):
            print 'WRFSounding(): wrfoutFile not found: ' + wrfoutFile
            allOK = False
        else:
            self._WRFOUT = wrfoutFile

        if stationLat < -90.0 or stationLat > 90.0:
            print 'WRFSounding(): Invalid stationLat: ' + str(stationLat)
            allOK = False
        else:
            self._stationLat = stationLat

        if stationLon < -180.0 or stationLon > 180.0:
            print 'WRFSounding(): Invalid stationLon: ' + str(stationLon)
            allOK = False
        else:
            self._stationLon = stationLon

        if allOK:
            print 'WRFSounding.py()'
            print '    wrfout file: ' + self._WRFOUT
            self._extractWRFSounding()

#-------------------------------------------------------------

    def _extractWRFSounding(self):

        # Create a temporary working directory
        wrkdirbasename = os.path.basename( os.tempnam() )
        wrkdirname = self._WORKING_DIR_ROOT + '/' + wrkdirbasename
        print 'WRFSounding(): Created wrkdirname: ' + wrkdirname
        os.mkdir( wrkdirname )


        # Change to the temporary working directory
        if os.path.isdir(wrkdirname):
            os.chdir(wrkdirname)
        else:
            print 'WRFSounding(): Temporary work directory not found: ' + wrkdirname
            sys.exit(1)



        # Create the command for running the NCL script, and run it
        # Create the name of NCL-generated ASCII WRF sounding file
        wrfSoundingFilename = wrkdirname + '/WRFSounding.txt'

        # Go ahead and extract the soundings - output will go in 
        # wrfSoundingFilename
        os.environ['NCARG_ROOT'] = self._NCARG_ROOT
        os.environ['POINT_LAT'] = str(self._stationLat)
        os.environ['POINT_LON'] = str(self._stationLon)

        os.environ['wrfoutFile'] = self._WRFOUT + '.nc'
        os.environ['asciiOutFile'] = wrfSoundingFilename 
        nclScript = self._NCL_EXTRACT_SCRIPT

        theCommand = self._NCL_COMMAND + ' ' + nclScript
        print 'WRFSounding(): Executing: ' + theCommand
        try:
            retcode = subprocess.call(theCommand, shell=True)
            if retcode < 0:
                print >>sys.stderr, "Child was terminated by signal", -retcode
            else:
                print >>sys.stderr, "Child returned", retcode
        except OSError, e:
            print >>sys.stderr, "Execution failed:", e

        # Extract the data from the NCL ASCII output and store it literally
        wrfSoundingFileHandle = open(wrfSoundingFilename, 'r')
        for line in wrfSoundingFileHandle:
            self._theRawAsciiSounding.append(line)
        wrfSoundingFileHandle.close()

        # Now, go ahead and store the data
        lineNum = 0
        for line in self._theRawAsciiSounding:
            if lineNum > 0:
                theTokens = line.split()
                # We expect 7 tokens.  If not present, skip to next line
                if len(theTokens) == 7:
                    self._z.append( float(theTokens[0]) )
                    self._P.append( float(theTokens[1]) )
                    self._T.append( float(theTokens[2]) )
                    self._Td.append( float(theTokens[3]) )
                    self._Qv.append( float(theTokens[4]) )
                    self._Wv.append( float(theTokens[5]) )
                    self._Wd.append( float(theTokens[6]) )
                else:
                    print 'WRFSounding(): incomplete sounding Level ' + \
                          lineNum + ', skipping...'
               

            lineNum += 1

#-------------------------------------------------------------

    def printASCIITable(self, outputFileHandle):

        for line in self._theRawAsciiSounding:
            outputFileHandle.write(line)

        
#-------------------------------------------------------------

    def getz(self):
        return self._z

    def getP(self):
        return self._P

    def getP(self):
        return self._P

    def getT(self):
        return self._T

    def getTd(self):
        return self._Td

    def getQv(self):
        return self._Qv

    def getWv(self):
        return self._Wv

    def getWd(self):
        return self._Wd

    def getNumLevels(self):
        return len(self._P)

    def getPBottom(self):
        # Returns pressure level of bottom layer in 
        # stored sounding
        if self.getNumLevels() > 0:
            return self._P[0]
        else:
            return 0

    def getPTop(self):
        # Returns pressure level of top layer in 
        # stored sounding
        if self.getNumLevels() > 0:
            return self._P[ len(self._P) - 1 ]
        else:
            return 0





#-------------------------------------------------------------
    



