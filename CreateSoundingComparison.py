

###############################################################
#                                                             #
#    Original Author contact information                      #
#                                                             #
#    Don Morton                                               #
#    Arctic Region Supercomputing Center                      #
#    University of Alaska Fairbanks                           #
#    Fairbanks, Alaska 99775                                  #
#    USA                                                      #
#                                                             #
#    Don.Morton@alaska.edu                                    #
#                                                             #
#    This software is presented freely to the public with     #
#    no restrictions on its use.  However, it would be        #
#    appreciated if any use of the software or methods in     #
#    part or in full acknowledges the source.                 #
#                                                             #
###############################################################



import os
import shutil
import subprocess
import sys

import LocalEnvironment
import RaobsSounding
import Stations
import WRFSounding


_GRAPHIC_EXTENSION = '.png'   # Extension/type of the final sounding graphic

def createSoundingGraphic(wrfRecord, raobsRecord, stationID, formattedTimestamp, productFile):

    # Assume timestamp is in a format that we want on the graphic
    # Verify that there's reasonable amount of data in the two records, 
    # and that productDir exists.

    # Note that it's up to the user to determine that the wrf and raobs records actually
    # pertain to the same time and location

    # Get the pressures from each record and insure we have at least 10 data points in each
    wrfP = wrfRecord.getP()
    if len(wrfP) < 10:
        print __name__ + '.createSoundingGraphic(): not enough WRF records'
        sys.exit(-1)
    raobsP = raobsRecord.getP()
    if len(raobsP) < 10:
        print __name__ + '.createSoundingGraphic(): not enough raobs records'
        sys.exit(-1)

    # Insure that product directory exists
    productDir = os.path.dirname(productFile)
    if not os.path.isdir(productDir):
        print __name__ + '.createSoundingGraphic(): product directory not found: ' + productDir
        sys.exit(-1)

    # If I've made it here, assume we're good to go - get the other sounding data
    wrfT = wrfRecord.getT()
    wrfTd = wrfRecord.getTd()
    wrfZ = wrfRecord.getz()
    wrfWv = wrfRecord.getWv()     ##########  CONVERT TO KNOTS  ###########
    wrfWd = wrfRecord.getWd()

    raobsT = raobsRecord.getT()
    raobsTd = raobsRecord.getTd()
    raobsZ = raobsRecord.getz()
    raobsWv = raobsRecord.getWv()   ############ OR CONVERT TO m/s  ##############
    raobsWd = raobsRecord.getWd()

    # Create a temporary directory for working in
    wrkdirbasename = os.path.basename( os.tempnam() )
    wrkdirname = LocalEnvironment.WORKING_DIR_ROOT + '/' + wrkdirbasename
    print __name__ + '.createSoundingGraphic(): Created wrkdirname: ' + wrkdirname
    os.mkdir( wrkdirname )

    # Write a simple ASCII file for each sounding that will be easy for the NCL 
    # script to read
    wrfASCIIFname = wrkdirname + '/wrf.txt'
    numWRFRecs = len(wrfP)
    os.environ['WRF_SOUNDING_FILE'] = wrfASCIIFname
    os.environ['WRF_NUM_LEVELS'] = str(numWRFRecs)
    wrfFH = open(wrfASCIIFname, 'w')
    idx = 0
    while idx < numWRFRecs:
        theLine = '%7.2f %5.1f %5.1f %5d %3d %3d\n' % \
                          (wrfP[idx], wrfT[idx], wrfTd[idx], \
                           wrfZ[idx], wrfWv[idx], wrfWd[idx])
        wrfFH.write(theLine)
        idx += 1
    wrfFH.close()

    raobsASCIIFname = wrkdirname + '/raobs.txt'
    numRaobsRecs = len(raobsP)
    os.environ['RAOBS_SOUNDING_FILE'] = raobsASCIIFname
    os.environ['RAOBS_NUM_LEVELS'] = str(numRaobsRecs)
    raobsFH = open(raobsASCIIFname, 'w')
    idx = 0
    while idx < numRaobsRecs:
        theLine = '%7.2f %5.1f %5.1f %5d %3d %3d\n' % \
                          (raobsP[idx], raobsT[idx], raobsTd[idx], \
                           raobsZ[idx], raobsWv[idx], raobsWd[idx])
        raobsFH.write(theLine)
        idx += 1

    raobsFH.close()

    # Set up for calling the NCL script
    os.chdir( wrkdirname )   # Make sure the image output goes into this dir
    os.environ['NCARG_ROOT'] = LocalEnvironment.NCARG_ROOT
    os.environ['PLOT_TITLE'] = stationID + '    ' + formattedTimestamp
    os.environ['PLOT_NAME'] = stationID 
    os.environ['GRAPHICS_TYPE'] = 'eps'
    theCommand = LocalEnvironment.NCL_COMMAND + ' ' + LocalEnvironment.NCL_SKEWT_SCRIPT
    print __name__ + '.createSoundingGraphic(): Executing ' + theCommand
    subprocess.call(theCommand, shell=True)

    # Convert the resulting graphic
    srcFile = wrkdirname + '/' + stationID + '.eps'  
    destFile = wrkdirname + '/' + stationID + _GRAPHIC_EXTENSION
    theCommand = LocalEnvironment.CONVERT_BIN + ' ' + srcFile + ' ' + destFile
    print __name__ + '.createSoundingGraphic(): Executing ' + theCommand
    subprocess.call(theCommand, shell=True)

    # Put the graphic in productDir
    if os.path.isfile(destFile):
        fname = os.path.basename(destFile)
        print __name__ + '.createSoundingGraphic(): Copying product to: ' + productFile
        shutil.copy(destFile, productFile)
    else:
        print __name__ + '.createSoundingGraphic(): Unable to find image file: ' + destFile

#-----------------------------------------------------------
#-----------------------------------------------------------
#-----------------------------------------------------------

def processSingleSounding(stationID, timestamp, wrfoutFile, productDir):

    "Creates tables for wrf and raobs sounding, and plots a comparison"

    # Assume stationID is the normal NWS ID (e.g. PAFA)
    # Assume timestamp is YYYYMMDDHH, where HH is 00 or 12
    # Assume wrfoutFile and productDir already exist

    # Error checking that follows will set this to False if any problems
    # are encountered
    OKToGo = True

    # Check the timestamp for validity (10 digits, HH is 00 or 12)
    if timestamp.isdigit and len(timestamp) == 10:
        hr = timestamp[8:10]
        if hr != '00' and hr != '12':
            print __name__ + '.processSingleSounding(): Bad timestamp Hour: ' + hr
            OKToGo = False
    else:
        print __name__ + '.processSingleSounding(): Bad timestamp: ' + timestamp
        OKToGo = False

    # Load up the list of stations
    stationsList = Stations.Stations()

    # Lookup stationID
    OKToGo = stationsList.isPresent(stationID)

    # Insure wrfout file is present
    if not os.path.isfile(wrfoutFile):
        print __name__ + '.processSingleSounding(): wrfoutFile not found: ' + wrfoutFile
        OKToGo = False

    # Insure productDir is present
    if not os.path.isdir(productDir):
        print __name__ + '.processSingleSounding(): productDir not found: ' + productDir
        OKToGo = False

    OUTPUT_FILE_PREFIX = timestamp + '_' + stationID + '_'

    if OKToGo:
        # Create the wrf sounding object and verify, create table and store
        lat = stationsList.latitude(stationID)
        lon = stationsList.longitude(stationID)
        wrfSound = WRFSounding.WRFSounding(wrfoutFile, lat, lon)
        if wrfSound.getNumLevels() > 5:
            fname = productDir + '/' + OUTPUT_FILE_PREFIX + 'wrfsound.txt'
            print __name__ + '.processSingleSounding(): Creating table : ' + fname
            fh = open(fname, 'w')
            wrfSound.printASCIITable(fh)
            fh.close()
        else:
            print __name__ + '.processSingleSounding(): Too few levels in WRF Sounding: ' + str(wrfSound.getNumLevels())
            OKToGo = False
    

    if OKToGo:
        # Create the raobs sounding object and verify, create table and store
        raobsID = stationsList.raobsID(stationID)
        raobsSound = RaobsSounding.RaobsSounding(raobsID, timestamp)
        if raobsSound.getNumLevels() > 5:
            fname = productDir + '/' + OUTPUT_FILE_PREFIX + 'raobssound.txt'
            print __name__ + '.processSingleSounding(): Creating table : ' + fname
            fh = open(fname, 'w')
            raobsSound.printASCIITable(fh)
            fh.close()
        else:
            print __name__ + '.processSingleSounding(): Too few levels in Raobs Sounding: ' + str(raobsSound.getNumLevels())
            OKToGo = False
    


    # So, if OKToGo is STILL True, then we probably have sufficient levels in
    # both the WRF and raobs sounding to create a graphic.
    if OKToGo:
        # Format the timestamp for the graphic
        graphicFile = productDir + '/' + OUTPUT_FILE_PREFIX + \
                      'skewt' + _GRAPHIC_EXTENSION
        formattedTimestamp = timestamp[0:4] + '-' + timestamp[4:6] + '-' + \
                             timestamp[6:8] + '_' + timestamp[8:10] 
        createSoundingGraphic(wrfSound, raobsSound, stationID, \
                              formattedTimestamp, graphicFile)


    # Print a departure message
    print __name__ + '.processSingleSounding(): ' + OUTPUT_FILE_PREFIX
    if OKToGo:
        print    'No problems detected in sounding comparison...'
    else:
        print    'Problems detected in sounding comparison...'
        
#-----------------------------------------------------------
#-----------------------------------------------------------
#-----------------------------------------------------------

def ForecastVerify(fcastStartTime, soundingTimes, stationID, 
                   nestNum, wrfoutDir, productDir):

    "Creates sounding comparisons for a specific station and forecast"

    # fcastStartTime - format YYYYMMDDHH
    # soundingTimes - list of times for comparison, format YYYYMMDDHH
    # stationID - common ID for station (e.g. PAFA)
    # nestNum - nest number to process (in case of multiple nests)
    # wrfoutDir - location of wrfout files - this routine will verify 
    #             presence of those needed
    # productDir - this routine assumes it already exists and will exit if not

    # Argument checking
    OKToGo = True
    if len(fcastStartTime) != 10 or not fcastStartTime.isdigit():
        print __name__ + '.ForecastVerify: bad fcastStartTime: ' + fcastStartTime
        OKToGo = False

    for theTime in soundingTimes:
        if len(theTime) != 10 or not theTime.isdigit():
            print __name__ + '.ForecastVerify: bad comparison time: ' + theTime
            OKToGo = False

    stationList = Stations.Stations()
    if not stationList.isPresent(stationID):
        print __name__ + '.ForecastVerify: bad stationID: ' + stationID
        OKToGo = False

    if int(nestNum) < 1 or int(nestNum) > 4:
        print __name__ + '.ForecastVerify: bad nestNum: ' + str(nestNum)
        OKToGo = False

    if not os.path.isdir(wrfoutDir):
        print __name__ + '.ForecastVerify: wrfoutDir not found: ' + wrfoutDir
        OKToGo = False
  
    if not os.path.isdir(productDir):
        print __name__ + '.ForecastVerify: productDir not found: ' + productDir
        OKToGo = False
  
    # If OKToGo is still True then, by golly, we're OK To Go.
    if OKToGo:

        # Start up an index.html for this forecast data
        htmlFH = open(productDir + '/index.html', 'w')

        startTimeFormatted = fcastStartTime[0:4] + '-' + \
                             fcastStartTime[4:6] + '-' + \
                             fcastStartTime[6:8] + '_' + \
                             fcastStartTime[8:10] + 'Z'
        htmlFH.write('<html>\n')
        htmlFH.write('<h1>Forecast Sounding Verification for ' + '&nbsp;' + \
                     '<u>' + stationID + '</u></h1>\n')
        htmlFH.write('<h2>' + 'Forecast Start Time:' + ' &nbsp; ' + 
                      startTimeFormatted + '</h2>\n')
        htmlFH.write('<br><br>\n')

        # Iterate through the comparison times 
        for theTime in soundingTimes:
            wrfoutFilename = wrfoutDir + '/wrfout_d'
            wrfoutFilename += '%02d' % nestNum
            wrfoutFilename += '_' + theTime[0:4] + '-' + theTime[4:6] + '-' + \
                             theTime[6:8] + '_' + theTime[8:10] + ':00:00'
            # Verify that we have a wrfout file, then process
            if os.path.isfile(wrfoutFilename):
                processSingleSounding(stationID, \
                    theTime, wrfoutFilename, productDir)

                ## Note that the file naming convention here MUST match that
                ## of processSingleSounding(), in this module.  Eventually,
                ## I should create a function that does this, so that all
                ## routines in here can access a single reference

                OUTPUT_FILE_PREFIX = theTime + '_' + stationID + '_'
                wrfTableFilename = OUTPUT_FILE_PREFIX + \
                                   'wrfsound.txt'
                raobsTableFilename = OUTPUT_FILE_PREFIX + \
                                     'raobssound.txt'
                graphicFilename = OUTPUT_FILE_PREFIX + \
                              'skewt' + _GRAPHIC_EXTENSION
            
                # Write an entry in the HTML file
                formattedTime = theTime[0:4] + '-' + theTime[4:6] + '-' + \
                             theTime[6:8] + '_' + theTime[8:10] + ':00:00'
                theEntry = "<b>" + formattedTime + "  </b><br>"
                theEntry += "<img src=\"" + graphicFilename + "\"><br>\n"
                theEntry += "<a href=\"" + wrfTableFilename + "\">[WRF Text Data]</a>"
                theEntry += " &nbsp; "
                theEntry += "<a href=\"" + raobsTableFilename + "\">[Raobs Text Data]</a>"
                theEntry += "<br><br><br>\n" 
                htmlFH.write(theEntry)
            else:
                # Write an entry in the HTML file
                formattedTime = theTime[0:4] + '-' + theTime[4:6] + '-' + \
                             theTime[6:8] + '_' + theTime[8:10] + ':00:00'
                theEntry = "<b>" + formattedTime + "  </b><br>"
                theEntry += "Comparison Unavailable<br>"
                theEntry += "<br><br><br>\n" 
                htmlFH.write(theEntry)



        htmlFH.write('</html>\n')
        htmlFH.close()

    else:
        # Create a default filler index.html
        htmlFH = open(productDir + '/index.html', 'w')
        htmlFH.write('<html>\n')
        htmlFH.write('<h1>Sorry, data is not currently available</h1>\n')
        htmlFH.write('</html>\n')
        htmlFH.close()
        

