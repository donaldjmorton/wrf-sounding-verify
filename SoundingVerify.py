#!/usr/bin/env python


###############################################################
#                                                             #
#    Original Author contact information                      #
#                                                             #
#    Don Morton                                               #
#    Arctic Region Supercomputing Center                      #
#    University of Alaska Fairbanks                           #
#    Fairbanks, Alaska 99775                                  #
#    USA                                                      #
#                                                             #
#    Don.Morton@alaska.edu                                    #
#                                                             #
#    This software is presented freely to the public with     #
#    no restrictions on its use.  However, it would be        #
#    appreciated if any use of the software or methods in     #
#    part or in full acknowledges the source.                 #
#                                                             #
###############################################################




import os
import os.path
import re
import sys
import time

import LocalEnvironment

import CreateSoundingComparison
import Stations

STATION_LIST = LocalEnvironment.STATION_LIST

### If START_TIME is blank, then it will be calculated based on command
### line arguments
START_TIME = LocalEnvironment.START_TIME

WRFOUT_ROOT_DIR = LocalEnvironment.WRFOUT_ROOT_DIR
NEST_NUMBER = LocalEnvironment.NEST_NUMBER

PRODUCT_ROOT_DIR = LocalEnvironment.PRODUCT_ROOT_DIR

#------------------------------------------

def main(argv=None):
    if argv is None:
        argv = sys.argv


    '''
    ############ Experiment with memory usage  ###################
    import resource
    print '****************************************************'
    print 'EXPERIMENTAL MEMORY STUFF IN SoundingVerify.py'
    print '****************************************************'
    (sas, has) = resource.getrlimit(resource.RLIMIT_AS)
    print 'initial soft as= ' + str(sas)
    print 'initial hard as= ' + str(has)

    resource.setrlimit( resource.RLIMIT_AS, (has,has) )

    (sas, has) = resource.getrlimit(resource.RLIMIT_AS)
    print 'new soft as= ' + str(sas)
    print 'new hard as= ' + str(has)
    print '****************************************************'
    print '****************************************************'
    '''


    global START_TIME
    # Get the arguments - Start hour and days offse
    if START_TIME is None:
        if len(sys.argv) == 3:
            startHour = int(sys.argv[1])
            dayOffset = int(sys.argv[2])
            START_TIME = calcStartTime(startHour, dayOffset)
        else:
            printUsage()
            sys.exit(1)


    print '---------------  GLOBAL VARIABLES --------------'
    print 'START_TIME                       : ' + START_TIME   
    print 'WRFOUT_ROOT_DIR                  : ' + WRFOUT_ROOT_DIR   
    print 'NEST_NUMBER                      : ' + str(NEST_NUMBER)   
    print 'PRODUCT_ROOT_DIR                  : ' + PRODUCT_ROOT_DIR   



    # Some basic error checking
    if not os.path.isdir(WRFOUT_ROOT_DIR):
        print 'SoundingVerify.py: Bad WRFOUT_ROOT_DIR: ' + WRFOUT_ROOT_DIR
        sys.exit(1)

    if not os.path.isdir(PRODUCT_ROOT_DIR):
        print 'SoundingVerify.py: Bad PRODUCT_ROOT_DIR: ' + PRODUCT_ROOT_DIR
        sys.exit(1)



    # Get a list of all wrfout files, then go through it to select times
    # of 00Z and 12Z for the sounding time list
    wrfoutDir = WRFOUT_ROOT_DIR + '/' + START_TIME
    if not os.path.isdir(wrfoutDir):
        print 'SoundingVerify.py: Unable to find wrfoutDir: ' + wrfoutDir
        sys.exit(1)

    wrfoutFileList = createFileList(wrfoutDir, NEST_NUMBER)
    soundingTimeList = []
    for file in wrfoutFileList:
        wrfoutTimeStr = file[11:24]
        ##print wrfoutTimeStr[11:13]
        if wrfoutTimeStr[11:13] in ['00', '12']:
            # Add this time to the timelist
            #print wrfoutTimeStr
            timeStr = wrfoutTimeStr[0:4] + wrfoutTimeStr[5:7] + \
                      wrfoutTimeStr[8:10] + wrfoutTimeStr[11:13]
            #print timeStr
            soundingTimeList.append(timeStr)

    # Abort if the sounding time list is empty
    if len(soundingTimeList) <= 0:
        print 'SoundingVerify.py: soundingTimeList is empty - aborting'
        sys.exit(1)


    # Next, create product directories - we'll work in hierarchy  of year,
    # month, and then forecastStartTime in the month subdirs, then for each
    # forecast, we'll have a stationID subdir, which will have the actual
    # comparison with HTML
    yearDir = PRODUCT_ROOT_DIR + '/' + START_TIME[0:4]
    if not os.path.isdir(yearDir):
        print 'Making directory: ' + yearDir
        os.mkdir(yearDir, 0755)

    monthDir = yearDir + '/' + START_TIME[4:6]
    if not os.path.isdir(monthDir):
        print 'Making directory: ' + monthDir
        os.mkdir(monthDir, 0755)

    formattedStartTime = START_TIME[0:4] + '-' + START_TIME[4:6] + '-' + \
                         START_TIME[6:8] + '_' + START_TIME[8:10]
    fcstDir = monthDir + '/' + formattedStartTime
    if not os.path.isdir(fcstDir):
        print 'Making directory: ' + fcstDir
        os.mkdir(fcstDir, 0755)



    # At this point, we should have a directory tree in which to place
    # this forecast.  So, now we iterate through the desired stations,
    # create the subdirectories, and run the processing
    stationInfo = Stations.Stations()
    for stationID in STATION_LIST:
        if stationInfo.isPresent(stationID):
            print 'Processing station: ' + stationID
            stationDir = fcstDir + '/' + stationID
            if not os.path.isdir(stationDir):
                print 'Making directory: ' + stationDir
                os.mkdir(stationDir, 0755)

            CreateSoundingComparison.ForecastVerify(START_TIME,          \
                                                    soundingTimeList,    \
                                                    stationID,           \
                                                    NEST_NUMBER,         \
                                                    wrfoutDir,           \
                                                    stationDir)

        else:
            print 'Skipping... stationID not found: ' + stationID
    
        
        



    
    





#------------------------------------------

def printUsage():
            print ' '
            print '        Usage      SoundingVerify.py sHour dOffset'
            print '                     sHour: fcast start hour 0-23'
            print '                     dOffset: Offset days (nonzero or negative)'
            print ' '

#------------------------------------------

def calcStartTime(fcstStartHour, dayOffset):

#  Input args  
#        fcstStartHour: start hour of forecast (from 0 to 23)
#        dayOffset    : Denotes number of days back or forward from current
#                       day (Zulu time).  Examples:
#
#                          dayOffset =  0: use current day (Zulu)
#                          dayOffset = -1: go back to previous day (Zulu)
#                          dayOffset = -2: go back two days (Zulu)
#
#  We will simply end up subtracting dayOffset*24 hours from the CURRENT
#  ZULU time, truncate at the day, then add fcstStartHour to that.  For
#  example.  If it's 2010-07-21_14:35:09Z, and we have arguments of 0 and -1,
#  will go back to 2010-07-20, and then append a start hour of 00:00:00 for a
#  start time of 2010-07-20_00:00:00.
#
#  We assume for now that we won't have a dayOffset greater than 0 (since the
#  raobs won't exist yet.
#
#  Function returns 'None' if there was a problem
#

    OKToGo = True
    if fcstStartHour < 0 or fcstStartHour > 23:
        print 'calcStartTime(): Bad forecast start hour: ' + str(fcstStartHour)
        printUsage()
        OKToGo = False

    if dayOffset > 0:
        print 'calcStartTime(): Bad day offset: ' + str(dayOffset)
        printUsage()
        OKToGo = False

    if OKToGo:
        # Assume we have good input - calculate start time
        currTime = time.time() # Seconds since Epoch 
        newTime = currTime + (dayOffset*86400)  # offset 24-hour increment
        newGMT = time.gmtime(newTime)   # Convert to GMT time structure

        newTimeStr = time.strftime("%Y%m%d", newGMT)  # Has correct YYYYMMDD
        newTimeStr += '%02d' % fcstStartHour  # Now has correct YYYYMMDDHH

        returnValue = newTimeStr
    else:
        returnValue = None

    return str(returnValue)


#------------------------------------------

#------------------------------------------------------------------

def createFileList(theDir, nestNum):

        # Get a list of files in "theDir" that match the pattern
        # "wrfout_d0<nestNum>_*"

        theList = []            # list of files which match the pattern
        wrfoutFileList = []     # List of wrfout files to process

        fullDirList = os.listdir(theDir)
        # print fullDirList

        # Iterate through the list, adding the ones that match the pattern
        # to "theList"
        for file in fullDirList:
                if re.search('wrfout_d0' + str(nestNum), file):
                        wrfoutFileList.append(file)

        wrfoutFileList.sort()

        return wrfoutFileList

#---------------------------------------------------






if __name__ == "__main__":
    main()

